function ResetCtrl($scope){
	$scope.reset = function(){
		chrome.storage.sync.clear();
		chrome.storage.local.clear();
		window.location.reload();
	};
};

function IgnoreListCtrl($scope){
	$scope.extensions = [];

	$scope.refresh = function() {
		async.waterfall([
			function (callback){
				chrome.management.getAll(function(all_extensions){
					callback(null, all_extensions);
				});
			},
			function (all_extensions, callback) {
				chrome.storage.sync.get("ignore_list", function(result){
					var ignore_list = result.hasOwnProperty("ignore_list") ? result.ignore_list : [];
					callback(null, all_extensions, ignore_list);
				});
			},
			function (all_extensions, ignore_list, callback) {
				var extensions = [];

				async.eachSeries(all_extensions, function(ext, cb){

					if(_.contains(ignore_list, ext.id)){
						ext.ignore = true;
					} else {
						ext.ignore = false;
					}

					if(ext.type === "extension" && ext.name != "Ext-Switch"){
						var new_index = (extensions.push(ext) - 1);
						cb(null);
					} else {
						cb(null);
					}


				}, function(err){
					callback(null, extensions);
				});
			}
		], function(err, result){
			if(err){
				throw err;
			}

			$scope.$apply(function(){
				$scope.extensions = result;
			});
		});
	};

	$scope.save = function(){
		console.log("Saving ignore list...")

		var new_ignore_list = [];

		async.eachSeries($scope.extensions, function(ext, cb){
			if(ext.ignore){
				new_ignore_list.push(ext.id);
				cb(null);
			} else {
				cb(null);
			}
		}, function(err){
			console.log(new_ignore_list);

			chrome.storage.sync.set({ ignore_list: new_ignore_list }, function(){
				console.log("Saved!")
			});
		});
	};
};
ext-switch
==========

A Chrome extension that disables/enables all extensions; with whitelist support.

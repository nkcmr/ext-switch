chrome.browserAction.onClicked.addListener(function receiveBrowserAction(tab) {
	async.waterfall([
		function (callback) {
			// Check current state of switch
			// 1 - all extensions are on; switch them off
			// 0 - all extensions are off; switch them on
			chrome.storage.local.get("switch_state", function(result){
				var state = result.hasOwnProperty("switch_state") ? result.switch_state : 1;

				callback(null, state);
			});
		},
		function (state, callback) {
			// Get the ignore list
			chrome.storage.sync.get("ignore_list", function(result){
				var ignore_list = result.ignore_list || [];

				callback(null, state, ignore_list);
			});
		},
		function (state, ignore_list, callback) {
			var setting = !state;
			try{
				chrome.management.getAll(function (all_extensions){
					async.eachSeries(all_extensions, function(ext, cb){
						if(ext.type === "extension" && ext.mayDisable && ext.name != "Ext-Switch" && !_.contains(ignore_list, ext.id)) {
							chrome.management.setEnabled(ext.id, setting);
							cb(null);
						}else {
							cb(null);
						}
					}, function(err){
						if(err) {
							callback(err);
							return;
						}

						callback(null, setting);
					});
				});
			}catch(e){
				callback(e);
			}
		}
	], function(err, state){
		if(err){
			console.error("Error occured whilst switching!");
			console.error("ERROR: %s", err.stack || err);
			return;
		}

		console.log("Extensions %s", state ? "enabled" : "disabled");
		chrome.storage.local.set({ switch_state: state }, function(){
			console.log("Switch state set to %s", state);
		});
	});
});